#
# Cookbook Name:: devops-challenge
# Recipe:: java
#
# Copyright (c) 2017 The Authors, All Rights Reserved.
if node['platform'] == 'ubuntu'
  execute "apt-update" do
    command "apt update"
  end
end

package 'Install Java' do
  case node[:platform]
  when 'redhat', 'centos'
    package_name 'java-1.8.0-openjdk'
  when 'ubuntu', 'debian'
    package_name 'openjdk-8-jdk'
  end
end
