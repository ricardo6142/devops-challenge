#
# Cookbook Name:: devops-challenge
# Recipe:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.

include_recipe "::java"

package 'Install Tomcat8' do
  case node[:platform]
  when 'redhat', 'centos'
    package_name 'tomcat'
  when 'ubuntu', 'debian'
    package_name 'tomcat8'
  end
end

# Deploy the webapp
case node['platform']
when 'debian', 'ubuntu'
  cookbook_file "/var/lib/tomcat8/webapps/java-artifact-chef-test.war" do
      source "java-chef-test.war"
      mode "0644"
      action :create
end
when 'redhat', 'centos', 'fedora'
  cookbook_file "/var/lib/tomcat/webapps/java-artifact-chef-test.war" do
    source "java-chef-test.war"
    mode "0644"
    action :create
end
end

# Start service
case node['platform']
when 'debian', 'ubuntu'
  service "tomcat8" do
      service_name "tomcat8"
      action :start
  end
when 'redhat', 'centos', 'fedora'
  service "tomcat" do
      service_name "tomcat"
      action :start
  end
end
