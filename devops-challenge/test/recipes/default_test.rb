# # encoding: utf-8

# Inspec test for recipe devops-challenge::default

# The Inspec reference, with examples and extensive documentation, can be
# found at https://docs.chef.io/inspec_reference.html

java_package = 'openjdk-8-jdk'
java_package = 'java-1.8.0-openjdk' if os[:family] == 'centos'

describe package(java_package) do
  it { should be_installed }
end

tomcat_package = 'tomcat8'
tomcat_package = 'tomcat' if os[:family] == 'centos'

describe package(tomcat_package) do
  it { should be_installed }
end

describe service(tomcat_package) do
  it { should be_running }
end 

describe port(8080) do
  it { should be_listening }
end

war_path = '/var/lib/tomcat8/webapps/java-artifact-chef-test.war'
war_path = '/var/lib/tomcat/webapps/java-artifact-chef-test.war' if os[:family] == 'centos'

describe file(war_path) do
  it { should exist }
end

describe command('wget -qO- http://localhost:8080/java-artifact-chef-test/chef/ping') do
  its('stdout') { should eq 'Hello Chef deployed war' }
end
